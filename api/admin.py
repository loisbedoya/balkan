""" Admin for module API """
from django.contrib import admin
from .models import Person, Attributes

admin.site.register(Person)
admin.site.register(Attributes)
