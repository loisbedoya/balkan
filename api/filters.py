""" Filters for module API """
import django_filters

from .models import Attributes

class AttributesFilter(django_filters.FilterSet):

    class Meta:
        model = Attributes
        fields = {
            'age': ['exact'],
            'happy': ['exact'],
            'healty': ['exact'],
            'busy': ['exact'],
        }
