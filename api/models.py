""" Models for module API """
from django.db import models


class Person(models.Model):
    first_name = models.CharField(max_length=100, null=False)
    last_name = models.CharField(max_length=100, null=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Attributes(models.Model):
    person = models.OneToOneField(Person, on_delete=models.CASCADE)
    age = models.IntegerField(blank=True, null=True)
    happy = models.BooleanField(blank=True, null=True)
    healty = models.BooleanField(blank=True, null=True)
    busy = models.BooleanField(blank=True, null=True)

    def __str__(self):
        return f"{self.person} {self.age}"
