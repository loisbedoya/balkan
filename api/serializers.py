""" Serializers for module API """
from rest_framework import serializers
from .models import Attributes


class PersonSerializer(serializers.Serializer):
    first_name = serializers.CharField(max_length=100)
    last_name = serializers.CharField(max_length=100)


class AttributesSerializer(serializers.HyperlinkedModelSerializer):
    person = PersonSerializer()

    class Meta:
        model = Attributes
        fields = '__all__'
        