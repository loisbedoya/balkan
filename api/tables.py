""" Tables for module API """
from django_tables2_column_shifter.tables import ColumnShiftTable
from .models import Attributes

class AttributesTable(ColumnShiftTable):
    class Meta:
        model = Attributes
        template_name = "django_tables2/bootstrap.html"
