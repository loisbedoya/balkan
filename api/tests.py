""" Unit tests for module API """
import json
from rest_framework.test import APITestCase, APIClient
from api.models import Person, Attributes


class AttributeTests(APITestCase):
    """
        Ensure we can create a new attribute object,
        access to the API and consult the object,
        filter objects
    """

    def tests(self):
        """
        Ensure we can create a new attribute object,
        access to the API and consult the object,
        filter objects
        """

        client = APIClient()

        #Creating some tests objects
        person = Person.objects.create(
            first_name='Julian',
            last_name='Camargo',
        )

        Attributes.objects.create(
            person=person,
            age=33,
            happy=False,
            healty=True,
            busy=True
        )

        person = Person.objects.create(
            first_name='Luisa',
            last_name='Bedoya',
        )

        Attributes.objects.create(
            person=person,
            age=28,
            happy=True,
            healty=False,
            busy=False
        )

        response = client.get('/attributes/')

        #The API works
        assert response.status_code == 200

        #Amount of objects
        result = json.loads(response.content)
        assert len(result) == 2

        #Testing Filters

        response = client.get('/attributes/?first_name=luisa')
        result = json.loads(response.content)
        assert len(result) == 1
        assert result[0]['person']['last_name'] == 'Bedoya'

        response = client.get('/attributes/?first_name=laura')
        result = json.loads(response.content)
        assert len(result) == 0

        response = client.get('/attributes/?first_name=luisa&age=10')
        result = json.loads(response.content)
        assert len(result) == 0

        response = client.get('/attributes/?first_name=luisa&age=28')
        result = json.loads(response.content)
        assert len(result) == 1
        assert result[0]['happy'] is True

        response = client.get('/attributes/?healty=true&busy=true')
        result = json.loads(response.content)
        assert len(result) == 1
        assert result[0]['person']['first_name'] == 'Julian'

        response = client.get('/attributes/?healty=trueTrue&busy=false')
        result = json.loads(response.content)
        assert len(result) == 0
