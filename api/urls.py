""" Urls for module API """
from django.urls import include, path
from rest_framework import routers
from .views import AttributesViewSet, FilteredAttributesListView

router = routers.DefaultRouter()
router.register(r'attributes', AttributesViewSet, 'attributes')

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path("attributes-table/", FilteredAttributesListView.as_view())
]
