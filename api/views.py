""" Views logic for module API """
from rest_framework import viewsets
from django_tables2 import SingleTableMixin
from django_filters.views import FilterView
from django.db.models import Q
from .serializers import AttributesSerializer
from .models import Attributes
from .tables import AttributesTable
from .filters import AttributesFilter



class AttributesViewSet(viewsets.ModelViewSet):
    serializer_class = AttributesSerializer

    def get_queryset(self):
        """
        Optionally restricts the returned attributes to a given parameter,
        by filtering against query parameter in the URL.
        """
        criterion1 = criterion2 = criterion3 = criterion4 = criterion5 = criterion6 = Q()
        queryset = Attributes.objects.all()

        first_name = self.request.query_params.get('first_name')
        last_name = self.request.query_params.get('last_name')
        age = self.request.query_params.get('age')
        happy = self.request.query_params.get('happy')
        healty = self.request.query_params.get('healty')
        busy = self.request.query_params.get('busy')

        if first_name:
            criterion1 = Q(person_id__first_name__iexact=first_name)
        if last_name:
            criterion2 = Q(person_id__last_name__iexact=last_name)
        if age:
            try:
                int(age)
                criterion3 = Q(age=age)
            except Exception:
                return []
        if happy:
            if happy.capitalize() in ['True', 'False']:
                criterion4 = Q(happy=happy.capitalize())
            else:
                return []
        if healty:
            if healty.capitalize() in ['True', 'False']:
                criterion5 = Q(healty=healty.capitalize())
            else:
                return []
        if busy:
            if busy.capitalize() in ['True', 'False']:
                criterion6 = Q(busy=busy.capitalize())
            else:
                return []

        return queryset.filter(criterion1 & criterion2 & criterion3
                             & criterion4 & criterion5 & criterion6)


class FilteredAttributesListView(SingleTableMixin, FilterView):
    model = Attributes
    table_class = AttributesTable
    template_name = 'api/attributes.html'
    filterset_class = AttributesFilter
